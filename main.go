package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Создаем мапу для соответствия римским цифрам
var RIMmap = map[string]int{
	"I": 1, "II": 2,
	"III": 3, "IV": 4,
	"V": 5, "VI": 6,
	"VII": 7, "VIII": 8,
	"IX": 9, "X": 10,
	"XL": 40, "L": 50,
	"XC": 90, "C": 100,
}

// Создаем мапу для соответствия арабским числам
var ARABbIvRIMmap = map[int]string{
	1: "I", 2: "II",
	3: "III", 4: "IV",
	5: "V", 6: "VI",
	7: "VII", 8: "VIII",
	9: "IX", 10: "X",
	40: "XL", 50: "L",
	90: "XC", 100: "C",
}

var a, b *int
var operators = map[string]func() int{
	"*": func() int { return *a * *b },
	"/": func() int { return *a / *b },
	"+": func() int { return *a + *b },
	"-": func() int { return *a - *b },
}

const (
	RIMminus  = "Вывод ошибки, так как в римской системе нет отрицательных чисел."
	RIMnoll   = "Вывод ошибки, так как в римской системе нет числа 0."
	NEFORMAT  = "Вывод ошибки, так как формат математической операции не удовлетворяет заданию — два операнда и один оператор (+, -, /, *)."
	NEMATH    = "Вывод ошибки, так как строка не является математической операцией."
	ARABvsRIM = "Вывод ошибки, так как используются одновременно разные системы счисления."
	CELdo10   = "Калькулятор умеет работать только с арабскими целыми числами или римскими цифрами от 1 до 10 включительно"
)

var data []string

/*
Создадим функцию для хранения операторов, в которой
будет цикл для перебора всех операторов.
*/
func headArguments(s string) {
	var DATAcashe string                  //Пустая переменная для хранения операторов
	var SCANstrings int                   //Для отслеживания кол-ва найденых строк
	RIMvint := make([]int, 0)             //Преобразованные римские в целые числа
	RIMLYNI := make([]string, 0)          //Для римских чисел
	CHISLA := make([]int, 0)              //Для чисел арабских
	for headOperator := range operators { //Цикл, переберающий все операторы
		for _, simvol := range s { //вложенный цикл, переберающий каждый символ строки S
			if headOperator == string(simvol) { //Проверочка текущего символа на тождество оператору
				DATAcashe += headOperator
				data = strings.Split(s, DATAcashe)
				/*Если условие выполняется, то добавляет оператор к переменной
				DATAcashe и разбиваем стороку "s" по оператору с помощью
				функции strings.Split. И как итог возращаем полученные данные
				*/
			}
		}
	}
	//Врубим переключатель для кейсов, и добавим в них вывод ошибок
	switch {
	case len(DATAcashe) > 1:
		panic(NEFORMAT) // Определение длинны объекта
	case len(DATAcashe) < 1:
		panic(NEMATH)
	}
	for _, elem := range data {
		// Преобразуем строку в целое число и проверим ее на возможность вида целого числа
		num, err := strconv.Atoi(elem)
		if err != nil {
			SCANstrings++
			RIMLYNI = append(RIMLYNI, elem)
		} else {
			CHISLA = append(CHISLA, num)
		}
	}
	// Проверим значения переменной SCANstrings
	switch SCANstrings {
	case 1:
		panic(ARABvsRIM)
	case 0:
		//Если равно 0, то выполняем проверку ошибок, используя условие(если истина,то блочится внутри кода if, в противном случаи ПАНИКА)
		errCheck := CHISLA[0] > 0 && CHISLA[0] < 11 &&
			CHISLA[1] > 0 && CHISLA[1] < 11
		if val, ok := operators[DATAcashe]; ok && errCheck == true {
			a, b = &CHISLA[0], &CHISLA[1]
			fmt.Println(val())
		} else {
			panic(CELdo10)
		}
	/*
		Если SCANstrings равно 2, то происходит итерация по элементам RIMLYNI.
		Если элемент может быть преобразован в число от 1 до 10, то это число
		добавляется в слайс RIMvint. Если это не так, то вызывается функция panic()
	*/
	case 2:
		for _, elem := range RIMLYNI {
			if val, ok := RIMmap[elem]; ok && val > 0 && val < 11 {
				RIMvint = append(RIMvint, val)
			} else {
				panic(CELdo10)
			}
		}
		if val, ok := operators[DATAcashe]; ok {
			a, b = &RIMvint[0], &RIMvint[1]
			intToRoman(val())
		}
	}
}
func ArabIvRIM(num int) string {
	// Список арабских чисел для транслейта
	diplomatARABOV := []int{
		100, 90, 50, 40, 10, 9,
		8, 7, 6, 5, 4, 3, 2, 1,
	}
	RIMNumeral := ""
	for num > 0 {
		for _, arabic := range diplomatARABOV {
			for num >= arabic {
				num -= arabic
				RIMNumeral += ARABbIvRIMmap[arabic]
			}
		}
	}

	return RIMNumeral
}
func intToRoman(itogRIM int) {
	var RIMNumeral string

	if itogRIM == 0 {
		panic(RIMnoll)
	} else if itogRIM < 0 {
		panic(RIMminus)
	} else {
		RIMNumeral = ArabIvRIM(itogRIM)
	}

	fmt.Println(RIMNumeral)
}

func main() {
	//Создадим приветствие при запуске программы с небольшим туториалом
	fmt.Println("            Доброго времени суток!\n Введите арифметическое выражение в одну строку:\n           Пример ввода (1+1; V+I) \n")
	//Создадим объект для чтения ввода в коносоль, для буферизиции потоков ввода используем пакет bufio
	//Для создания потока ввода применяем функцию bufio.NewReader с объектом os.Stdin
	vvodInf := bufio.NewReader(os.Stdin)
	//Создадим цикл, внутри которого считывается строка ввода пользователя
	for {
		console, _ := vvodInf.ReadString('\n')
		s := strings.ReplaceAll(console, " ", "")
		headArguments(strings.ToUpper(strings.TrimSpace(s)))
	}
}
